﻿using System;

namespace Task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter text:");

            string text = Console.ReadLine();

            Console.WriteLine("\nChoose number of color for text: ");

            ConsoleColor[] colors = (ConsoleColor[])ConsoleColor.GetValues(typeof(ConsoleColor));

            for(int i = 0; i < colors.Length; i++)
                Console.WriteLine($"\t{i}   :   {colors[i]}");

            Console.Write("  >> ");

            int color = int.Parse(Console.ReadLine());

            new Test().Print(text, color);
        }
    }
}
