﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class Test
    {
        public void Print(string stroka, int color)
        {
            Console.ForegroundColor = (ConsoleColor) color;
            Console.WriteLine(stroka);
            Console.ResetColor();
        }
    }
}
