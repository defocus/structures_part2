﻿using System;

namespace Task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Choose employe:");

            EnumEmployes[] employes = (EnumEmployes[])EnumEmployes.GetValues(typeof(EnumEmployes));

            for(int i = 0; i < employes.Length; i++)
                Console.WriteLine($"\t{i}  :  {employes[i]}");

            Console.Write("  >> ");

            int item = int.Parse(Console.ReadLine());

            Console.WriteLine($"Give a bonus {employes[item]}: {new Accountant().AskForBonus(employes[item], new Random().Next(100, 200))}");
        }
    }
}
